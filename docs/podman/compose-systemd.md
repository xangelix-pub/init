```bash
sudo loginctl enable-linger $USER
sudo podman-compose systemd --action create-unit
```

This will create a unit file with @ magic like this `podman-compose@<PROJECT>`

To register a service for your compose stack, cd into that directory and type

`podman-compose systemd --action register`

Now you can use systemd like this

```bash
systemctl --user enable --now 'podman-compose@{PROJ}'
systemctl --user start 'podman-compose@{PROJ}'
systemctl --user stop 'podman-compose@{PROJ}'
systemctl --user status 'podman-compose@{PROJ}'
journalctl --user -xeu 'podman-compose@{PROJ}'
```

It works like this

`/usr/lib/systemd/user/podman-compose@.service`

```
[Unit]
Description=%i rootless pod (podman-compose)

[Service]
Type=simple
EnvironmentFile=%h/.config/containers/compose/projects/%i.env
ExecStartPre=-/usr/bin/podman-compose up --no-start
ExecStartPre=/usr/bin/podman pod start pod_%i
ExecStart=/usr/bin/podman-compose wait
ExecStop=/usr/bin/podman pod stop pod_%i

[Install]
WantedBy=default.target
```

The register command publish the env files in your home `EnvironmentFile=%h/.config/containers/compose/projects/%i.env`
which is used to create the pod without starting the containers, then it's started and waited.
