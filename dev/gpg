#!/usr/bin/env bash

# Init keys from env vars
RSA_KEY_LOC="$HOME/.ssh/id_rsa"
if test -f "$RSA_KEY_LOC"; then
  echo "$RSA_KEY_LOC exists."
else
  if [[ -n "${SSH_KEY_B64}" ]]; then
    echo $SSH_KEY_B64 | base64 -di > $RSA_KEY_LOC
  else
    echo Variable SSH_KEY_B64 does not exist.
  fi
fi

GPG_KEY_LOC="$HOME/pkey.key"
if test -f "$GPG_KEY_LOC"; then
  echo "$GPG_KEY_LOC exists."
else
  if [[ -n "${GPG_KEY_B64}" ]]; then
    echo $GPG_KEY_B64 | base64 -di > $GPG_KEY_LOC
  else
    echo Variable GPG_KEY_B64 does not exist.
  fi
fi

# Stage 1
echo "INFO: Setting SSH private key permissions."
sudo chmod 600 $RSA_KEY_LOC

until echo "INFO: Please enter your SSH key password:" && echo "ssh-keygen -y -f $RSA_KEY_LOC > $(dirname $RSA_KEY_LOC)/id_rsa.pub"
do
  echo "FAIL: SSH Key Password Entry Failed"
  echo "Retrying..."
done

# Stage 2
until echo "INFO: Please enter your PGP key password:" && gpg --pinentry-mode=loopback --import $GPG_KEY_LOC
do
  echo "FAIL: PGP Password Entry #1 Failed"
  echo "Retrying..."
done

echo "STAT: GPG private key imported"

git config --global user.signingkey $GPG_KEY_ID
git config --global commit.gpgsign true
git config --global user.email $GIT_EMAIL
echo "STAT: GPG private key ID and email set in git"

test -r $HOME/.bash_profile && echo 'export GPG_TTY=$(tty)' >> $HOME/.bash_profile
echo 'export GPG_TTY=$(tty)' >> $HOME/.profile
echo "STAT: GPG private key set in bash"

printf "allow-loopback-pinentry\ndefault-cache-ttl 34560000\nmax-cache-ttl 34560000\ndefault-cache-ttl-ssh 34560000\nmax-cache-ttl-ssh 34560000" >> $HOME/.gnupg/gpg-agent.conf
echo "pinentry-mode loopback" >> $HOME/.gnupg/gpg.conf
gpg-connect-agent reloadagent /bye
touch temp.txt
echo "STAT: Prepared for signing test"

# Stage 3
until echo "INFO: Please enter your PGP key password:" && gpg --pinentry-mode=loopback --sign temp.txt
do
  echo "FAIL: PGP Password Entry #2 Failed"
  echo "Retrying..."
done

rm -f temp.txt temp.txt.gpg
echo "DONE: GPG initialization workaround complete"
