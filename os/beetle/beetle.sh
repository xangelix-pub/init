#!/usr/bin/env sh

# Check correct argc
argc=6
if [[ $# -ne $argc ]]; then
  echo "Incorrect number of arguments, $# should be $argc."
  exit 1
fi

# Set argv
cert=$1
email=$2
account=$3
token=$4
zone=$5
domain=$6

# Resolve hold dir

if grep 1000 /etc/passwd; then
  user=$(grep 1000 /etc/passwd | cut -d ':' -f 1)
else
  user=$(grep 33333 /etc/passwd | cut -d ':' -f 1)
fi
beetle_dir="/home/$user/.hold/beetle/"

# Verify hold dir exists
runuser -l $user -c "mkdir -p ${beetle_dir}cloudflared"

# Detect distro
which pacman > /dev/null 2>&1
if [[ "$?" == "1" ]]; then
  echo "Distribution not supported."
  exit 1
else
  distro="archlinux"
fi

# Verify cloudflared install
which cloudflared > /dev/null 2>&1
if [[ "$?" == "1" ]]; then
  if [[ "$distro" == "archlinux" ]]; then
    pacman -Syu --noconfirm cloudflared
  fi
fi

# Download cert
if [[ ! -f "${beetle_dir}cloudflared/cert.pem" ]]; then
  mkdir /root/.cloudflared
  curl -Lo /root/.cloudflared/cert.pem "https://drive.google.com/uc?export=download&id=$cert"
fi

# Create and set tunnel
tunnel=$(cloudflared tunnel create $(echo $RANDOM | md5sum | head -c 20) | grep json | cut -d '/' -f 4 | cut -d '.' -f 1)
mv "/root/.cloudflared/$tunnel.json" "${beetle_dir}cloudflared/"

# Set cert
mv /root/.cloudflared/cert.pem "${beetle_dir}cloudflared/cert.pem"

# Download and set tunnel config
file="cloudflared/config.yml"
curl "https://gitlab.com/xangelix-pub/init/-/raw/main/os/beetle/$file" | sed 's/<tunnel_id>/'"$tunnel"'/g' | sed 's/<domain>/'"$domain"'/g' > "$beetle_dir$file"

# Download and set podman config
file="podman-compose.yml"
curl "https://gitlab.com/xangelix-pub/init/-/raw/main/os/beetle/$file" > "$beetle_dir$file"

# Verify cloudflared uninstall
if [[ "$distro" == "archlinux" ]]; then
  pacman -R --noconfirm cloudflared
fi

# Set other request attributes
type="CNAME"
name=$(echo $domain | sed -e 's/\.*[^\.]*\.[^\.]*$//')
content="$tunnel.cfargotunnel.com"
comment="auto:beetle"

# Create DNS CNAME record
request1=$(curl -X POST "https://api.cloudflare.com/client/v4/zones/$zone/dns_records" \
  -H "Authorization: Bearer $token" \
  -H "Content-Type: application/json" \
  --data '{"type": "'"$type"'", "name": "'"$name"'", "content": "'"$content"'","ttl": 1, "proxied": true, "comment": "'"$comment"'"}')

# Create SSH application
request2=$(curl -X POST "https://api.cloudflare.com/client/v4/accounts/$account/access/apps" \
  -H "Authorization: Bearer $token" \
  -H "Content-Type: application/json" \
  --data '{"type":"ssh","name":"'"$domain"'","domain":"'"$domain"'"}')

# Get SSH application ID
uuid=$(echo $request2 | grep '"id"' | cut -d '"' -f 6)

# Create policy to allow only email on SSH application
request3=$(curl -X POST "https://api.cloudflare.com/client/v4/accounts/$account/access/apps/$uuid/policies" \
  -H "Authorization: Bearer $token" \
  -H "Content-Type: application/json" \
  --data '{"precedence": 1, "decision": "allow", "name": "Allow Me", "include": [{"email":{"email": "'"$email"'"}}]}')

# Prefer podman, root-less, systemd-less runtime
cd $beetle_dir

which podman-compose > /dev/null 2>&1
if [[ "$?" == "1" ]]; then
  which docker-compose > /dev/null 2>&1
  if [[ "$?" == "1" ]]; then
    echo "No container runtime could be started!"
    exit 1
  else
    docker-compose up -d -f podman-compose.yml
  fi
else
  runuser -l $user -c "podman-compose up -d"
fi

# Shred on exit
shred -u $0
