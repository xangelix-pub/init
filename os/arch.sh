#!/usr/bin/env sh

# Fetch default user
user=$(grep 1000 /etc/passwd | cut -d ':' -f 1)

# Temporarily disable sudo pass on user
if [[ -f "/etc/sudoers.d/00_$user" ]]; then
  sed -i "s!tux ALL=(ALL) ALL!tux ALL=(ALL) NOPASSWD: ALL!" /etc/sudoers.d/00_$user
fi

# Init pacman keys
runuser -l $user -c "sudo pacman-key --init"

# Add chaotic-aur pacman repo keys and mirrorlist
runuser -l $user -c "sudo pacman-key --recv-key 3056513887B78AEB --keyserver keyserver.ubuntu.com && \
sudo pacman-key --lsign-key 3056513887B78AEB && \
sudo pacman -U --noconfirm \
'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' \
'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'"

# Add cachyos pacman repo keys and mirrorlist
runuser -l $user -c "sudo pacman-key --recv-keys F3B607488DB35A47 --keyserver keyserver.ubuntu.com && \
sudo pacman-key --lsign-key F3B607488DB35A47 && \
sudo pacman -U --noconfirm 'https://mirror.cachyos.org/repo/x86_64/cachyos/cachyos-keyring-20240331-1-any.pkg.tar.zst' \
'https://mirror.cachyos.org/repo/x86_64/cachyos/cachyos-mirrorlist-18-1-any.pkg.tar.zst' \
'https://mirror.cachyos.org/repo/x86_64/cachyos/cachyos-v3-mirrorlist-18-1-any.pkg.tar.zst' \
'https://mirror.cachyos.org/repo/x86_64/cachyos/cachyos-v4-mirrorlist-6-1-any.pkg.tar.zst' \
'https://mirror.cachyos.org/repo/x86_64/cachyos/pacman-7.0.0.r6.gc685ae6-2-x86_64.pkg.tar.zst'"

# Add arch4edu pacman repo keys and mirrorlist
runuser -l $user -c "pacman-key --recv-keys 7931B6D628C8D3BA && \
pacman-key --finger 7931B6D628C8D3BA && \
pacman-key --lsign-key 7931B6D628C8D3BA && \
sudo pacman -U --noconfirm 'https://mirrors.tuna.tsinghua.edu.cn/arch4edu/any/arch4edu-keyring-20200805-1-any.pkg.tar.zst'"

# Custom pacman.conf
curl -O https://gitlab.com/xangelix-pub/init/-/raw/main/os/pacman.conf && mv pacman.conf /etc/pacman.conf
sudo chmod 644 /etc/pacman.conf

# Update all packages and install AUR helper
pacman -Syu --noconfirm paru

# Install fundamental utilities
runuser -l $user -c "paru -Syu --noconfirm bind inetutils openssh rsync git-lfs cmake ninja"

# Setup git lfs
runuser -l $user -c "sudo git lfs install --system"

# Install additional misc utilities
runuser -l $user -c "paru -Syu --noconfirm fastfetch ttf-cascadia-code-nerd podman buildah fuse-overlayfs nano fish starship ripgrep-all fd sd"
runuser -l $user -c "mkdir -p ~/.config/fish/ && echo -e 'if status is-interactive\n    # Commands to run in interactive sessions can go here\nend\nstarship init fish | source' >> ~/.config/fish/config.fish"
chsh -s /usr/sbin/fish $user
chsh -s /usr/sbin/fish root

# Set paru.conf options
sd "#BottomUp" "BottomUp" /etc/paru.conf
sd "#SudoLoop" "SudoLoop" /etc/paru.conf

# Set makepkg.conf options
sd '#MAKEFLAGS="-j2"' 'MAKEFLAGS="-j$(nproc)"' /etc/makepkg.conf

# Re-enable sudo pass on user
if [[ -f "/etc/sudoers.d/00_$user" ]]; then
  sd "tux ALL=(ALL) NOPASSWD: ALL" "tux ALL=(ALL) ALL!" /etc/sudoers.d/00_$user
fi
