use lambda_web::actix_web::{web, Responder, Result};
use serde::{Deserialize, Serialize};
//use serde_json::{self, json};

#[derive(Deserialize)]
pub struct RegisterRequest {
    pub email: String,
}

#[derive(Serialize)]
pub struct RegisterResponse {
    captcha: String,
    time_exp: String,
}

#[derive(Deserialize)]
pub struct VerifyRequest {
    pub email: String,
    pub captcha_solution: String,
}

#[derive(Serialize, Deserialize)]
pub struct UserLock {
    lock_type: String,
    time_now: String,
    time_exp: String,
    state: String,
    data: String,
}

#[derive(Serialize)]
struct VerifyResponse {
    status: String,
}

use ring::{digest};
use serde_json::{self, json};
use redis::aio::Connection;
use redis::JsonAsyncCommands;
use redis::RedisResult;
use crate::proc::captcha::get_captcha;
use crate::proc::expiration::get_time_pair;
use crate::proc::expiration::TimePair;
use crate::proc::db::get_db_connection;

use serde_json::Value;

use data_encoding::HEXLOWER;

pub async fn email_to_user_id(email: String) -> String {
    HEXLOWER.encode(digest::digest(&digest::SHA512, email.as_bytes()).as_ref())
}

pub async fn register_user_obj(email: String, typ: String, captcha_solution: String) -> (Value, TimePair) {
    let state = if typ == "admin" { "valid" } else { "registering" };
    let tp: TimePair = get_time_pair(1).await;
    (json!({"email": email, "type": typ, "state": state, "locks": [{"time_now": tp.time_now, "time_exp": tp.time_exp, "lock_type": "register", "state": state, "data": captcha_solution}]}), tp)
}
pub async fn add_user(email: String, typ: String) -> RegisterResponse {
    let mut con: Connection = get_db_connection().await;
    let user_id = email_to_user_id(email.to_string()).await;

    let c = get_captcha().await;

    let (user, tp) = &register_user_obj(email.to_string(), typ.to_string(), c.captcha_solution).await;
    let _res: RedisResult<String> = con.json_set(format!("users.{user_id}"), "$", user).await;

    RegisterResponse {
        captcha: c.captcha,
        time_exp: tp.time_exp.to_string(),
    }
}

pub async fn ep_client_account_register(register_request: web::Json<RegisterRequest>) -> Result<impl Responder> {
    let response = add_user(register_request.email.to_string(), "user".to_string()).await;
    Ok(web::Json(response))
}

async fn db_check_captcha(user_id: String, captcha_solution: String) -> bool {
    let mut db_con: Connection = get_db_connection().await;
    let a: RedisResult<String> = db_con.json_get(format!("users.{user_id}"), "$.locks[0]").await;
    let b: serde_json::Result<Vec<UserLock>> = serde_json::from_str(a.expect("n").as_str());
    let json = b.expect("wow");

    json[0].data == captcha_solution
}

pub async fn ep_client_account_verify(verify_request: web::Json<VerifyRequest>) -> Result<impl Responder> {
    let user_id = email_to_user_id(verify_request.email.to_string()).await;
    let captcha_solution = verify_request.captcha_solution.to_string();

    let verify_result = db_check_captcha(user_id, captcha_solution).await;

    let response = VerifyResponse {
        status: if verify_result { "good".to_string() } else { "bad".to_string() }
    };

    Ok(web::Json(response))
}
