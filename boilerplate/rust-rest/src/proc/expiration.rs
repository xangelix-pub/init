use time::{
    format_description::well_known::Iso8601,
    OffsetDateTime,
    Duration
};

pub struct TimePair {
    pub time_now: String,
    pub time_exp: String,
}

pub async fn get_time_pair(hours: i64) -> TimePair {
    let time = OffsetDateTime::now_utc();

    TimePair {
        time_now: time.format(&Iso8601::DEFAULT).expect("woof"),
        time_exp: time.saturating_add(Duration::hours(hours)).format(&Iso8601::DEFAULT).expect("woof")
    }
}
