use redis::aio::Connection;

pub async fn get_db_connection() -> Connection {
    let db_client: redis::Client = redis::Client::open("redis://127.0.0.1/").unwrap();
    let db_con = db_client.get_async_connection().await;
    db_con.expect("woof")
}
