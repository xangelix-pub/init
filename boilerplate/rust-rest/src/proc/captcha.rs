use captcha::filters::{Cow, Noise, Wave};
use captcha::{Captcha, Geometry};

use rand::Rng;

pub struct CaptchaPair {
    pub captcha: String,
    pub captcha_solution: String,
}

pub async fn get_captcha() -> CaptchaPair {
    let mut c = Captcha::new();
    //println!("{:?}", c.supported_chars());
    let mut rng = rand::thread_rng();
    c.set_chars(&['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'k', 'm', 'n', 'o', 'p', 'r', 's', 'u', 'w', 'x', 'y', 'z', '2', '3', '5', '6', '7', '8'])
        .add_chars(8)
        .apply_filter(Noise::new(0.2))
        .apply_filter(Wave::new(2.0, 20.0))
        .view(220, 120)
        .apply_filter(
            Cow::new()
                .min_radius(40)
                .max_radius(50)
                .circles(1)
                .area(Geometry::new(40, 150, 50, 70)),
        )
        .set_color([rng.gen_range(25..=255), rng.gen_range(25..=255), rng.gen_range(25..=255)]);

    CaptchaPair {
        captcha: c.as_base64().expect("woofllll"),
        captcha_solution: c.chars_as_string(),
    }
}
