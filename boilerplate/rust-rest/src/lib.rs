use log::{info};
use lambda_web::actix_web::{self, web, App, HttpServer, Result};
use lambda_web::{is_running_on_lambda, run_actix_on_lambda, LambdaError};
use listenfd::ListenFd;
use std::env;

mod routes;
pub mod proc;

#[actix_web::main]
pub async fn serve() -> Result<(), LambdaError> {
    env_logger::init();
    routes::client::v1::accounts::add_user("neiman@cody.to".to_string(), "admin".to_string()).await;

    let mut listenfd = ListenFd::from_env();

    let factory = move || {
        App::new()
            .route("/client/v1/account/register", web::post().to(routes::client::v1::accounts::ep_client_account_register))
            .route("/client/v1/account/verify", web::post().to(routes::client::v1::accounts::ep_client_account_verify))
    };

    if is_running_on_lambda() {
        // Run on AWS Lambda
        run_actix_on_lambda(factory).await.expect("warn");
    } else {
        // Local server
        let mut server = HttpServer::new(factory).bind("127.0.0.1:5580")?;

        server = match listenfd.take_tcp_listener(0)? {
            Some(listener) => server.listen(listener)?,
            None => {
                let host = env::var("HOST").expect("Host not set");
                let port = env::var("PORT").expect("Port not set");
                server.bind(format!("{host}:{port}"))?
            }
        };

        info!("Starting server");
        server.run().await?;
    }
    Ok(())
}
