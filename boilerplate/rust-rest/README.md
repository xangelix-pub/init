# rust-rest

migrations

if version does not exist
  create migration status
  set version
  create user array
  set migration status





user lock

api.<base_domain>/client/v1/account/register

<--
{
  "email": "<email>"
}

-->
{
  "captcha": "<image>",
  "expiration": "<time> + 1h"
}

if redis.users[email] does not exist or redis.users[email].state[0] == registering
  redis.users[email].state set_state {type: registering, date: date, status: active, data: {type: captcha, value: captcha_text}}
  upload captcha
  return s3 captcha link
else
  return error: user already exists


api.<base_domain>/client/v1/account/verify

<--
{
  "email": "<email>",
  "captcha": "<captcha_text>"
}

-->
{
  blank
}

if state registering
  if captcha correct
    set user state to valid


api.<base_domain>/client/v1/account/login

<--
{
  "email": "<email>"
}

-->
{
  "expiration": "<time> + 1h"
}

if user state valid
  set user state login
  upload captcha
  send email with captcha


api.<base_domain>/client/v1/account/token

<--
{
  "email": "<email>",
  "captcha": "<token>"
}

-->
{
  "token": "<token>",
  "expiration": "<time> + 7d"
}

if user state login
  if captcha correct
    kill state
    generate token
    add token to user
    return token


