#!/usr/bin/env sh

project_type=$1
project_name=$2

find "./boilerplate/$project_type/" -type f -exec sed -i 's/<project_name>/'"$project_name"'/g' {} \;
